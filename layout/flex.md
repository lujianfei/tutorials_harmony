## 弹性布局（Flex）

```typescript


import router from '@ohos.router'
import { CheckSourceButton } from '../../common/CheckSourceButton'
@Entry
@Component
@Preview
struct FlexPage {

  @State title:string = ""
  @State urlPath:string = ""

  aboutToAppear() {
    let param = router.getParams()
    this.title = param["title"]
    this.urlPath = param["urlPath"]
  }

  build() {
      Navigation() {
          List() {
            // FlexDirection.Row
            ListItem() {
              Text() {
                Span('Flex').fontSize(15).fontColor('#ff04cafa')
                Span('({ ').fontSize(15).fontColor('#fffa9804')
                Span('direction: FlexDirection.Row').fontSize(15).fontColor('#ff0000')
                Span(' })').fontSize(15).fontColor('#fffa9804')
              }.padding('10vp')
            }
            ListItem() {
              Flex({ direction: FlexDirection.Row }) {
                Text('1').width('33%').height(50).backgroundColor(0xF5DEB3).alignSelf(ItemAlign.Center).textAlign(TextAlign.Center)
                Text('2').width('33%').height(50).backgroundColor(0xD2B48C).alignSelf(ItemAlign.Center).textAlign(TextAlign.Center)
                Text('3').width('33%').height(50).backgroundColor(0xF5DEB3).alignSelf(ItemAlign.Center).textAlign(TextAlign.Center)
              }
              .height(70)
              .width('100%')
              .padding(10)
              .backgroundColor(0xAFEEEE)
            }
            // FlexDirection.Column
            ListItem() {
              Text() {
                Span('Flex').fontSize(15).fontColor('#ff04cafa')
                Span('({ ').fontSize(15).fontColor('#fffa9804')
                Span('direction: FlexDirection.Column').fontSize(15).fontColor('#ff0000')
                Span(' })').fontSize(15).fontColor('#fffa9804')
              }.padding('10vp')
            }
            ListItem() {
              Flex({ direction: FlexDirection.Column }) {
                Text('1').width('33%').height(50).backgroundColor(0xF5DEB3).alignSelf(ItemAlign.Center).textAlign(TextAlign.Center)
                Text('2').width('33%').height(50).backgroundColor(0xD2B48C).alignSelf(ItemAlign.Center).textAlign(TextAlign.Center)
                Text('3').width('33%').height(50).backgroundColor(0xF5DEB3).alignSelf(ItemAlign.Center).textAlign(TextAlign.Center)
              }
              .height(170)
              .width('100%')
              .padding(10)
              .backgroundColor(0xAFEEEE)
            }
            // FlexDirection.RowReverse
            ListItem() {
              Text() {
                Span('Flex').fontSize(15).fontColor('#ff04cafa')
                Span('({ ').fontSize(15).fontColor('#fffa9804')
                Span('direction: FlexDirection.RowReverse').fontSize(15).fontColor('#ff0000')
                Span(' })').fontSize(15).fontColor('#fffa9804')
              }.padding('10vp')
            }
            ListItem() {
              Flex({ direction: FlexDirection.RowReverse }) {
                Text('1').width('33%').height(50).backgroundColor(0xF5DEB3).alignSelf(ItemAlign.Center).textAlign(TextAlign.Center)
                Text('2').width('33%').height(50).backgroundColor(0xD2B48C).alignSelf(ItemAlign.Center).textAlign(TextAlign.Center)
                Text('3').width('33%').height(50).backgroundColor(0xF5DEB3).alignSelf(ItemAlign.Center).textAlign(TextAlign.Center)
              }
              .height(70)
              .width('100%')
              .padding(10)
              .backgroundColor(0xAFEEEE)
            }
            // FlexDirection.ColumnReverse
            ListItem() {
              Text() {
                Span('Flex').fontSize(15).fontColor('#ff04cafa')
                Span('({ ').fontSize(15).fontColor('#fffa9804')
                Span('direction: FlexDirection.ColumnReverse').fontSize(15).fontColor('#ff0000')
                Span(' })').fontSize(15).fontColor('#fffa9804')
              }.padding('10vp')
            }
            ListItem() {
              Flex({ direction: FlexDirection.ColumnReverse }) {
                Text('1').width('33%').height(50).backgroundColor(0xF5DEB3).alignSelf(ItemAlign.Center).textAlign(TextAlign.Center)
                Text('2').width('33%').height(50).backgroundColor(0xD2B48C).alignSelf(ItemAlign.Center).textAlign(TextAlign.Center)
                Text('3').width('33%').height(50).backgroundColor(0xF5DEB3).alignSelf(ItemAlign.Center).textAlign(TextAlign.Center)
              }
              .height(170)
              .width('100%')
              .padding(10)
              .backgroundColor(0xAFEEEE)
            }
            // FlexDirection.ColumnReverse
            ListItem() {
              Text() {
                Span('Flex').fontSize(15).fontColor('#ff04cafa')
                Span('({ ').fontSize(15).fontColor('#fffa9804')
                Span('wrap: FlexWrap.NoWrap').fontSize(15).fontColor('#ff0000')
                Span(' })').fontSize(15).fontColor('#fffa9804')
              }.padding('10vp')
            }
            ListItem() {
              Flex({ wrap: FlexWrap.NoWrap }) {
                Text('1').width('50%').height(50).backgroundColor(0xF5DEB3).alignSelf(ItemAlign.Center).textAlign(TextAlign.Center)
                Text('2').width('50%').height(50).backgroundColor(0xD2B48C).alignSelf(ItemAlign.Center).textAlign(TextAlign.Center)
                Text('3').width('50%').height(50).backgroundColor(0xF5DEB3).alignSelf(ItemAlign.Center).textAlign(TextAlign.Center)
              }
              .width('100%')
              .padding(10)
              .backgroundColor(0xAFEEEE)
            }
            // FlexWrap.Wrap
            ListItem() {
              Text() {
                Span('Flex').fontSize(15).fontColor('#ff04cafa')
                Span('({ ').fontSize(15).fontColor('#fffa9804')
                Span('wrap: FlexWrap.Wrap').fontSize(15).fontColor('#ff0000')
                Span(' })').fontSize(15).fontColor('#fffa9804')
              }.padding('10vp')
            }
            ListItem() {
              Flex({ wrap: FlexWrap.Wrap }) {
                Text('1').width('50%').height(50).backgroundColor(0xF5DEB3).textAlign(TextAlign.Center)
                Text('2').width('50%').height(50).backgroundColor(0xD2B48C).textAlign(TextAlign.Center)
                Text('3').width('50%').height(50).backgroundColor(0xD2B48C).textAlign(TextAlign.Center)
              }
              .width('100%')
              .padding(10)
              .backgroundColor(0xAFEEEE)
            }
            // FlexWrap.WrapReverse
            ListItem() {
              Text() {
                Span('Flex').fontSize(15).fontColor('#ff04cafa')
                Span('({ ').fontSize(15).fontColor('#fffa9804')
                Span('wrap: FlexWrap.WrapReverse').fontSize(15).fontColor('#ff0000')
                Span(' })').fontSize(15).fontColor('#fffa9804')
              }.padding('10vp')
            }
            ListItem() {
              Flex({ wrap: FlexWrap.WrapReverse }) {
                Text('1').width('50%').height(50).backgroundColor(0xF5DEB3).textAlign(TextAlign.Center)
                Text('2').width('50%').height(50).backgroundColor(0xD2B48C).textAlign(TextAlign.Center)
                Text('3').width('50%').height(50).backgroundColor(0xD2B48C).textAlign(TextAlign.Center)
              }
              .width('100%')
              .height(150)
              .padding(10)
              .backgroundColor(0xAFEEEE)
            }
            ListItem() {
              CheckSourceButton({url:this.urlPath, title: this.title})
            }.margin({bottom: 15, top : 15, right: 15})
          }
          .backgroundColor('#ffffffff')
      }
      .title(this.title)
      .hideToolBar(true)
      .hideBackButton(false)
    }
}
```