## 页面转场动画


### Page A
```typescript

import router from '@ohos.router'
import { CheckSourceButton } from '../../common/CheckSourceButton'
@Entry
@Component
@Preview
struct PageTransitionPage {

  @State title:string = ""
  @State urlPath:string = ""

  aboutToAppear() {
    let param = router.getParams()
    this.title = param["title"]
    this.urlPath = param["urlPath"]

  }

  pageTransition() {
    PageTransitionEnter({ type: RouteType.None, duration: 1200 })
      .slide(SlideEffect.Left)
    PageTransitionExit({ type: RouteType.None, duration: 1000 })
      .slide(SlideEffect.Left)
  }
  

  build() {
      Navigation() {
          Column() {
            Text("Page A").fontSize(25).margin(10)
            Button('跳转 Page B')
              .onClick(() => {
                  router.push({
                    url: "pages/anim/page_transition_dest",
                    params: {
                      title:'Page B'
                    }
                  })
              })
            CheckSourceButton({url:this.urlPath, title: this.title})
          }.width('100%').height('100%')
      }
      .title(this.title)
      .hideToolBar(true)
      .hideBackButton(false)
    }
}
```

### Page B
```typescript
import router from '@ohos.router'
import { CheckSourceButton } from '../../common/CheckSourceButton'
@Entry
@Component
@Preview
struct PageTransitionDestPage {

  @State title:string = ""
  @State urlPath:string = ""

  aboutToAppear() {
    let param = router.getParams()
    this.title = param["title"]
    this.urlPath = param["urlPath"]

  }

  pageTransition() {
    PageTransitionEnter({ type: RouteType.None, duration: 1000 })
      .slide(SlideEffect.Right)
    PageTransitionExit({ type: RouteType.None, duration: 1200 })
      .slide(SlideEffect.Right)
  }
  

  build() {
      Navigation() {
          Column() {
            Button('返回 Page A')
              .onClick(() => {
                  router.back()
              })
          }.width('100%').height('100%')
      }
      .title(this.title)
      .hideToolBar(true)
      .hideBackButton(false)
    }
}
```
