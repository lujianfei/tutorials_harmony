## 文本显示（Text/Span）

```typescript

import router from '@ohos.router'
import { CheckSourceButton } from '../../common/CheckSourceButton'
@Entry
@Component
@Preview
struct TextPage {

  @State title:string = ""
  @State urlPath:string = ""

  aboutToAppear() {
    let param = router.getParams()
    this.title = param["title"]
    this.urlPath = param["urlPath"]
  }

  build() {
      Navigation() {
        List({space:10}) {
          ListItem() {
            Text(){
              Span('Text').fontSize(15).fontColor('#ff04cafa')
              Span(`(`).fontSize(15).fontColor('#ff0000')
              Span('\'我是一段文本\'').fontSize(15)
              Span(`)`).fontSize(15).fontColor('#ff0000')
            }
          }.margin({top:10})
          ListItem() {
            Text('我是一段文本')
          }.margin({top:5})
          ListItem() {
            Text(){
              Span('Text').fontSize(15).fontColor('#ff04cafa')
              Span(`(`).fontSize(15).fontColor('#ff0000')
              Span('$r(\'app.string.module_desc\')').fontSize(15)
              Span(`)`).fontSize(15).fontColor('#ff0000')
            }
          }.margin({top:10})
          ListItem() {
            Text($r('app.string.module_desc'))
              .baselineOffset(0)
              .fontSize(30)
              .border({ width: 1 })
              .padding(10)
              .width(300)
          }.margin({top:5})

          ListItem() {
            Text("Span 的用法").fontSize(20)
          }.margin({top:10})
          ListItem() {
            Text() {
              Span('我是Span1，').fontSize(16).fontColor(Color.Grey)
                .decoration({ type: TextDecorationType.LineThrough, color: Color.Red })
              Span('我是Span2').fontColor(Color.Blue).fontSize(16)
                .fontStyle(FontStyle.Italic)
                .decoration({ type: TextDecorationType.Underline, color: Color.Black })
              Span('，我是Span3').fontSize(16).fontColor(Color.Grey)
                .decoration({ type: TextDecorationType.Overline, color: Color.Green })
            }
            .borderWidth(1)
            .padding(10)
          }.margin({top:5})

          ListItem() {
            Text("通过 textAlign 属性设置文本对齐样式").fontSize(20)
          }.margin({top:10})
          ListItem() {
            Column() {
              Text('左对齐')
                .width(300)
                .textAlign(TextAlign.Start)
                .border({ width: 1 })
                .padding(10)
              Text('中间对齐')
                .width(300)
                .textAlign(TextAlign.Center)
                .border({ width: 1 })
                .padding(10)
              Text('右对齐')
                .width(300)
                .textAlign(TextAlign.End)
                .border({ width: 1 })
                .padding(10)
            }
          }.margin({top:5})

          ListItem() {
            Text("通过 textOverflow 属性控制文本超长处理，textOverflow 需配合 maxLines 一起使用（默认情况下文本自动折行）").fontSize(20)
          }.margin({top:10})
          ListItem() {
            Column() {
              Text('This is the setting of textOverflow to Clip text content This is the setting of textOverflow to None text content. This is the setting of textOverflow to Clip text content This is the setting of textOverflow to None text content.')
                .width(250)
                .textOverflow({ overflow: TextOverflow.None })
                .maxLines(1)
                .fontSize(12)
                .border({ width: 1 }).padding(10)
              Text('我是超长文本，超出的部分显示省略号。I am an extra long text, with ellipses displayed for any excess。')
                .width(250)
                .textOverflow({ overflow: TextOverflow.Ellipsis })
                .maxLines(1)
                .fontSize(12)
                .border({ width: 1 }).padding(10)
            }
          }.margin({top:5})

          ListItem() {
            Text("通过 lineHeight 属性设置文本行高").fontSize(20)
          }.margin({top:10})
          ListItem() {
            Column() {
              Text('This is the text with the line height set. This is the text with the line height set.')
                .width(300).fontSize(12).border({ width: 1 }).padding(10)
              Text('This is the text with the line height set. This is the text with the line height set.')
                .width(300).fontSize(12).border({ width: 1 }).padding(10)
                .lineHeight(20)
            }
          }.margin({top:5})

          ListItem() {
            Text("通过 decoration 属性设置文本装饰线样式及其颜色").fontSize(20)
          }.margin({top:10})
          ListItem() {
            Column() {
              Text('This is the text')
                .decoration({
                  type: TextDecorationType.LineThrough,
                  color: Color.Red
                })
                .borderWidth(1).padding(10).margin(5)
              Text('This is the text')
                .decoration({
                  type: TextDecorationType.Overline,
                  color: Color.Red
                })
                .borderWidth(1).padding(10).margin(5)
              Text('This is the text')
                .decoration({
                  type: TextDecorationType.Underline,
                  color: Color.Red
                })
                .borderWidth(1).padding(10).margin(5)
            }
          }.margin({top:5})

          ListItem() {
            Text("通过 baselineOffset 属性设置文本基线的偏移量").fontSize(20)
          }.margin({top:10})
          ListItem() {
            Column() {
              Text('This is the text content with baselineOffset 0.')
                .baselineOffset(0)
                .fontSize(12)
                .border({ width: 1 })
                .padding(10)
                .width('100%')
                .margin(5)
              Text('This is the text content with baselineOffset 30.')
                .baselineOffset(30)
                .fontSize(12)
                .border({ width: 1 })
                .padding(10)
                .width('100%')
                .margin(5)

              Text('This is the text content with baselineOffset -20.')
                .baselineOffset(-20)
                .fontSize(12)
                .border({ width: 1 })
                .padding(10)
                .width('100%')
                .margin(5)
            }
          }.margin({top:5})

          ListItem() {
            Text("通过 letterSpacing 属性设置文本字符间距").fontSize(20)
          }.margin({top:10})
          ListItem() {
            Column() {
              Text('This is the text content with letterSpacing 0.')
                .letterSpacing(0)
                .fontSize(12)
                .border({ width: 1 })
                .padding(10)
                .width('100%')
                .margin(5)
              Text('This is the text content with letterSpacing 3.')
                .letterSpacing(3)
                .fontSize(12)
                .border({ width: 1 })
                .padding(10)
                .width('100%')
                .margin(5)
              Text('This is the text content with letterSpacing -1.')
                .letterSpacing(-1)
                .fontSize(12)
                .border({ width: 1 })
                .padding(10)
                .width('100%')
                .margin(5)
            }
          }.margin({top:5})



          ListItem() {
            Text("通过 minFontSize 与 maxFontSize 自适应字体大小，minFontSize 设置文本最小显示字号，maxFontSize 设置文本最大显示字号，minFontSize 与 maxFontSize 必须搭配同时使用，以及需配合 maxline 或布局大小限制一起使用，单独设置不生效。").fontSize(15)
          }.margin({top:10})
          ListItem() {
            Column() {
              Text('我的最大字号为30，最小字号为5，宽度为250，maxLines为1')
                .width(250)
                .maxLines(1)
                .maxFontSize(30)
                .minFontSize(5)
                .border({ width: 1 })
                .padding(10)
                .margin(5)
              Text('我的最大字号为30，最小字号为5，宽度为250，maxLines为2')
                .width(250)
                .maxLines(2)
                .maxFontSize(30)
                .minFontSize(5)
                .border({ width: 1 })
                .padding(10)
                .margin(5)
              Text('我的最大字号为30，最小字号为15，宽度为250,高度为50')
                .width(250)
                .height(50)
                .maxFontSize(30)
                .minFontSize(15)
                .border({ width: 1 })
                .padding(10)
                .margin(5)
              Text('我的最大字号为30，最小字号为15，宽度为250,高度为100')
                .width(250)
                .height(100)
                .maxFontSize(30)
                .minFontSize(15)
                .border({ width: 1 })
                .padding(10)
                .margin(5)
            }
          }.margin({top:5})

          ListItem() {
            Text("通过 textCase 属性设置文本大小写").fontSize(20)
          }.margin({top:10})
          ListItem() {
            Column() {
              Text('This is the text content with textCase set to Normal.')
                .textCase(TextCase.Normal)
                .padding(10)
                .border({ width: 1 })
                .padding(10)
                .margin(5)

              // 文本全小写展示
              Text('This is the text content with textCase set to LowerCase.')
                .textCase(TextCase.LowerCase)
                .border({ width: 1 })
                .padding(10)
                .margin(5)

              // 文本全大写展示
              Text('This is the text content with textCase set to UpperCase.')
                .textCase(TextCase.UpperCase)
                .border({ width: 1 })
                .padding(10)
                .margin(5)
            }
          }.margin({top:5})

          ListItem() {
            Text("Text 组件可以添加通用事件，可以绑定 onClick、onTouch 等事件来响应操作")
          }.margin({top:10})
          ListItem() {
            Text('点我')
              .onClick(()=>{
                console.info('我是Text的点击响应事件');
              })
          }.margin({top:5})

          ListItem() {
            CheckSourceButton({url:this.urlPath, title: this.title})
          }.margin({bottom: 15, top : 15, right: 15})
        }
        .padding(10)
      }
      .title(this.title)
      .hideToolBar(true)
      .hideBackButton(false)
    }
}
```