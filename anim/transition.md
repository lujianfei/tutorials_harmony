## 组件内转场动画

```typescript

import router from '@ohos.router'
import { CheckSourceButton } from '../../common/CheckSourceButton'
@Entry
@Component
@Preview
struct TransitionPage {

  @State title:string = ""
  @State urlPath:string = ""

  @State flag: boolean = true;
  @State show: string = 'show';

  aboutToAppear() {
    let param = router.getParams()
    this.title = param["title"]
    this.urlPath = param["urlPath"]
  }

  build() {
      Navigation() {
          Column() {
            Button(this.show).width(80).height(30).margin(30)
              .onClick(() => {
                if (this.flag) {
                  this.show = 'hide';
                } else {
                  this.show = 'show';
                }
                // 点击Button控制Image的显示和消失
                animateTo({ duration: 1000 }, () => {
                  this.flag = !this.flag;
                })
              })
            if (this.flag) {
              // Image的出现和消失配置为不同的过渡效果
              Image($r('app.media.fengjing')).width(200).height(200)
                .transition({ type: TransitionType.Insert, translate: { x: 200, y: -200 } })
                .transition({ type: TransitionType.Delete, opacity: 0, scale: { x: 0, y: 0 } })
            }
            CheckSourceButton({url:this.urlPath, title: this.title})
          }.width('100%').height('100%')
      }
      .title(this.title)
      .hideToolBar(true)
      .hideBackButton(false)
    }
}
```