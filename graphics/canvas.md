## 使用画布绘制自定义图形（Canvas）

```typescript

import router from '@ohos.router'
import { CheckSourceButton } from '../../common/CheckSourceButton'
@Entry
@Component
@Preview
struct CanvasPage {

  @State title:string = ""
  @State urlPath:string = ""

  //用来配置CanvasRenderingContext2D对象和OffscreenCanvasRenderingContext2D对象的参数，包括是否开启抗锯齿。true表明开启抗锯齿
  private settings: RenderingContextSettings = new RenderingContextSettings(true)
  private context: CanvasRenderingContext2D = new CanvasRenderingContext2D(this.settings)

  aboutToAppear() {
    let param = router.getParams()
    this.title = param["title"]
    this.urlPath = param["urlPath"]
  }

  build() {
      Navigation() {
          Stack() {
            Canvas(this.context)
              .width('100%')
              .height('100%')
              .backgroundColor('#000000')
              .onReady(() =>{
                //可以在这里绘制内容
                let y = 15
                // 绘制无填充正方形
                this.context.strokeStyle = '#FFFFFF'
                this.context.strokeRect(50, y, 100, 100);
                // 绘制蓝色正方形
                this.context.fillStyle = '#0097D4';
                this.context.fillRect(170, y, 100, 100);
                // 画线
                y += 120
                this.context.strokeStyle = '#FF0000'
                this.context.beginPath();
                this.context.moveTo(50, y);
                this.context.lineTo(150, 250);
                this.context.stroke();
                let region = new Path2D();
                // 画圆
                y += 50
                this.context.strokeStyle = '#0000FF'
                // arc(x: number, y: number, radius: number, startAngle: number, endAngle: number, counterclockwise?: boolean): void;
                region.arc(220, y, 50, 0, 6.28);
                this.context.stroke(region)
                // 文字
                y += 100
                this.context.font = '50px sans-serif';
                this.context.fillText("Hello World!", 50, y);
              })
             CheckSourceButton({url:this.urlPath, title: this.title})
          }
      }
      .title(this.title)
      .hideToolBar(true)
      .hideBackButton(false)
    }
}
```