## 创建列表（List）

```typescript
import router from '@ohos.router'
import { CheckSourceButton } from '../../common/CheckSourceButton'
@Entry
@Component
@Preview
struct ListPage {

  @State title:string = ""
  @State urlPath:string = ""

  @State dataList:string[] = []

  aboutToAppear() {
    let param = router.getParams()
    this.title = param["title"]
    this.urlPath = param["urlPath"]

    for (let index = 0; index < 30; index++) {
      this.dataList.push(`Item ${index + 1}`)
    }
  }

  build() {
      Navigation() {
          List() {
            ForEach(this.dataList, (item:string)=> {
              ListItem() {
                Text(item)
                  .fontSize(20)
                  .padding('10vp')
              }
            })
            ListItem() {
              CheckSourceButton({url:this.urlPath, title: this.title})
            }.margin({bottom: 15, top : 15, right: 15})
          }
          .backgroundColor('#ffffffff')
      }
      .title(this.title)
      .hideToolBar(true)
      .hideBackButton(false)
    }
}
```