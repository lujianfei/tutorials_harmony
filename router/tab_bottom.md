## Tabs ({barPosition:BarPosition.End}) 底部导航栏

```typescript

import router from '@ohos.router'
import { JumpSourceUtils } from '../../common/JumpSourceUtils'
@Entry
@Component
@Preview
struct TabPage {

  @State title:string = ""
  @State urlPath:string = ""

  aboutToAppear() {
    let param = router.getParams()
    this.title = param["title"]
    this.urlPath = param["urlPath"]
  }

  build() {
      Navigation() {
        Tabs({barPosition:BarPosition.End}) {
          TabContent() {
            Column() {
              Text('首页的内容').width('100%').textAlign(TextAlign.Center).fontSize(30)
              Text('查看源码->').width('100%').textAlign(TextAlign.Center).fontSize(25).onClick(()=>{
                JumpSourceUtils.jump(this.urlPath, this.title)
              }).margin({top:20}).fontColor($r('app.color.check_source_font_color'))
            }.width('100%').height('100%').justifyContent(FlexAlign.Center)
          }
          .tabBar('首页')

          TabContent() {
            Text('推荐的内容').width('100%').height('100%').textAlign(TextAlign.Center).fontSize(30)
          }
          .tabBar('推荐')

          TabContent() {
            Text('发现的内容').width('100%').height('100%').textAlign(TextAlign.Center).fontSize(30)
          }
          .tabBar('发现')

          TabContent() {
            Text('我的内容').width('100%').height('100%').textAlign(TextAlign.Center).fontSize(30)
          }
          .tabBar("我的")
        }
      }
      .title(this.title)
      .hideToolBar(true)
      .hideBackButton(false)
    }
}
```