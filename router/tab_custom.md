## Tabs ({barPosition:BarPosition.End}) 自定义底部导航栏

```typescript

import router from '@ohos.router'
import { JumpSourceUtils } from '../../common/JumpSourceUtils'
@Entry
@Component
@Preview
struct TabPage {

  @State title:string = ""
  @State urlPath:string = ""

  @State currentIndex:number = 0
  private tabsController : TabsController = new TabsController()

  aboutToAppear() {
    let param = router.getParams()
    this.title = param["title"]
    this.urlPath = param["urlPath"]
  }

  @Builder TabBuilder(title: string, targetIndex: number) {
    Column() {
      Text(title)
        .fontColor(this.currentIndex === targetIndex ? '#1698CE' : '#6B6B6B')
        .fontSize(this.currentIndex === targetIndex ? 20:17)
        .width('100%')
        .height('100%')
        .textAlign(TextAlign.Center)
    }
    .onClick(() => {
      this.currentIndex = targetIndex;
      this.tabsController.changeIndex(this.currentIndex);
    })
  }

  build() {
      Navigation() {
        Tabs({barPosition:BarPosition.End, controller:this.tabsController}) {
          TabContent() {
            Column() {
              Text('首页的内容').width('100%').textAlign(TextAlign.Center).fontSize(30)
              Text('查看源码->').width('100%').textAlign(TextAlign.Center).fontSize(25).onClick(()=>{
                JumpSourceUtils.jump(this.urlPath, this.title)
              }).margin({top:20}).fontColor($r('app.color.check_source_font_color'))
            }.width('100%').height('100%').justifyContent(FlexAlign.Center)
          }
          .tabBar(this.TabBuilder('首页', 0))

          TabContent() {
            Text('推荐的内容').width('100%').height('100%').textAlign(TextAlign.Center).fontSize(30)
          }
          .tabBar(this.TabBuilder('推荐', 1))

          TabContent() {
            Text('发现的内容').width('100%').height('100%').textAlign(TextAlign.Center).fontSize(30)
          }
          .tabBar(this.TabBuilder('发现', 2))

          TabContent() {
            Text('我的内容').width('100%').height('100%').textAlign(TextAlign.Center).fontSize(30)
          }
          .tabBar(this.TabBuilder("我的", 3))
        }.onChange((index:number)=> {
          this.currentIndex = index
        })
      }
      .title(this.title)
      .hideToolBar(true)
      .hideBackButton(false)
    }
}
```