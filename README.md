# 鸿蒙 Harmony 通关手册
## 学习目录 

### 一、鸿蒙 Harmony 初体验

### 二、UI 开发

#### 2.1 布局
##### 线性布局 (Row/Column)
##### 层叠布局 (Stack)
##### 弹性布局（Flex）
##### 相对布局（RelativeContainer）
##### 栅格布局（GridRow/GridCol）
##### 媒体查询（mediaquery）
##### 创建列表（List）
##### 创建网格（Grid/GridItem）
##### 创建轮播（Swiper）

#### 2.2 组件

##### 按钮（Button）
##### 单选框（Radio）
##### 切换按钮（Toggle）
##### 进度条（Progress）
##### 文本显示（Text/Span）
##### 文本输入（TextInput/TextArea）
##### 自定义弹窗（CustomDialog）
##### 视频播放（Video）
##### XComponent
##### 气泡提示（Popup）
##### 菜单（Menu）

#### 2.3 页面路由和组件导航
##### 页面路由（router）
##### Navigation
##### Tabs

#### 2.4 显示图形
##### 显示图片（Image）
##### 绘制几何图形（Shape）
##### 使用画布绘制自定义图形（Canvas）

#### 2.5 使用动画
##### 布局更新动画（显式动画，属性动画）
##### 组件内转场动画
##### 弹簧曲线动画
##### 放大缩小视图
##### 页面转场动画

#### 2.6 交互事件
##### 触屏事件
##### 键鼠事件
##### 焦点事件
##### 绑定手势方法
##### 单一手势
##### 组合手势

### 未完待续
https://developer.harmonyos.com/cn/docs/documentation/doc-guides-V3/arkts-drawing-customization-on-canvas-0000001453684976-V3