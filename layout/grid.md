## 创建网格（Grid/GridItem）

```typescript

import router from '@ohos.router'
import { CheckSourceButton } from '../../common/CheckSourceButton'
@Entry
@Component
@Preview
struct GridPage {

  @State title:string = ""
  @State urlPath:string = ""

  @State dataList:string[] = []

  aboutToAppear() {
    let param = router.getParams()
    this.title = param["title"]
    this.urlPath = param["urlPath"]

    for (let index = 0; index < 30; index++) {
      this.dataList.push(`Item ${index + 1}`)
    }
  }

  build() {
      Navigation() {
        Grid() { // 网格布局子选项
          ForEach(this.dataList, (item:string)=> {
            GridItem() { // 网格布局子选项
              Text(item)
                .alignSelf(ItemAlign.Center) // 垂直居中
                .textAlign(TextAlign.Center) // 水平居中
                .width('100%') // 100% 宽度
                .aspectRatio(1) // 宽高比
                .backgroundColor('#ff90b6db') // 背景色
            }
          })

          GridItem() {
            CheckSourceButton({url:this.urlPath, title: this.title})
          }
          .columnStart(1)
          .columnEnd(2)
          .margin({bottom: 15, top : 15, right: 15})
        }
        .columnsTemplate('1fr 1fr') // 两列，列宽权重 1：1
        .rowsGap(10) // 行间距
        .columnsGap(10) // 列间距
      }
      .title(this.title)
      .hideToolBar(true)
      .hideBackButton(false)
    }
}
```