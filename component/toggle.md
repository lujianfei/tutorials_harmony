## 切换按钮（Toggle）

```typescript

import router from '@ohos.router'
import { CheckSourceButton } from '../../common/CheckSourceButton'
@Entry
@Component
@Preview
struct TogglePage {

  @State title:string = ""
  @State urlPath:string = ""

  @State checkbox1:boolean = true
  @State checkbox2:boolean = false
  @State checkbox3:boolean = false
  @State checkbox4:boolean = false

  aboutToAppear() {
    let param = router.getParams()
    this.title = param["title"]
    this.urlPath = param["urlPath"]
  }

  build() {
      Navigation() {
        List({space:10}) {
          ListItem() {
            Text(){
              Span('Checkbox1').fontSize(15)
              Span(` ${this.checkbox1}`).fontSize(15).fontColor('#ff0000')
              Span('\tCheckbox2').fontSize(15)
              Span(` ${this.checkbox2}`).fontSize(15).fontColor('#ff0000')
            }
          }.margin({top:10})
          ListItem() {
            Text(){
              Span('Checkbox3').fontSize(15)
              Span(` ${this.checkbox3}`).fontSize(15).fontColor('#ff0000')
              Span('\tCheckbox4').fontSize(15)
              Span(` ${this.checkbox4}`).fontSize(15).fontColor('#ff0000')
            }
          }.margin({top:10})
          ListItem() {
            Row() {
              Toggle({ type: ToggleType.Checkbox, isOn: this.checkbox1 }).width(20).height(20)
                .onChange((isOn: boolean) => {
                  if (isOn) {
                    this.checkbox1 = true
                  } else {
                    this.checkbox1 = false
                  }
                })
              Text('Checkbox1').fontSize(15).onClick(() => {
                this.checkbox1 = !this.checkbox1
              })
              Toggle({ type: ToggleType.Checkbox, isOn: this.checkbox2 }).width(20).height(20)
                .onChange((isOn: boolean) => {
                  if (isOn) {
                    this.checkbox2 = true
                  } else {
                    this.checkbox2 = false
                  }
                })
              Text('Checkbox2').fontSize(15).onClick(() => {
                this.checkbox2 = !this.checkbox2
              })
            }
          }
          ListItem() {
            Row() {
              Toggle({ type: ToggleType.Switch, isOn: this.checkbox3 }).width(40).height(20)
                .onChange((isOn: boolean) => {
                  if (isOn) {
                    this.checkbox3 = true
                  } else {
                    this.checkbox3 = false
                  }
                })
              Text('Checkbox3').fontSize(15).onClick(() => {
                this.checkbox3 = !this.checkbox3
              })
              Toggle({ type: ToggleType.Switch, isOn: this.checkbox4 }).width(40).height(20)
                .onChange((isOn: boolean) => {
                  if (isOn) {
                    this.checkbox4 = true
                  } else {
                    this.checkbox4 = false
                  }
                })
              Text('Checkbox4').fontSize(15).onClick(() => {
                this.checkbox4 = !this.checkbox4
              })
            }.margin({ bottom: 20 })
          }
          ListItem() {
            CheckSourceButton({url:this.urlPath, title: this.title})
          }.margin({bottom: 15, top : 15, right: 15})
        }
        .padding(10)
      }
      .title(this.title)
      .hideToolBar(true)
      .hideBackButton(false)
    }
}
```