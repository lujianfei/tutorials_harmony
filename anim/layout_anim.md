## 布局更新动画

```typescript

import router from '@ohos.router'
import { CheckSourceButton } from '../../common/CheckSourceButton'
@Entry
@Component
@Preview
struct LayoutAnimPage {

  @State title:string = ""
  @State urlPath:string = ""

  // 用于控制Column的alignItems属性
  @State itemAlign: HorizontalAlign = HorizontalAlign.Start;
  allAlign: HorizontalAlign[] = [HorizontalAlign.Start, HorizontalAlign.Center, HorizontalAlign.End];
  alignIndex: number = 0;


  @State myWidth: number = 100;
  @State myHeight: number = 50;
  // 标志位，true和false分别对应一组myWidth、myHeight值
  @State flag: boolean = false;

  scroller: Scroller = new Scroller()

  aboutToAppear() {
    let param = router.getParams()
    this.title = param["title"]
    this.urlPath = param["urlPath"]
  }

  build() {
      Navigation() {
        Scroll(this.scroller) {
          Column() {
            // 对齐动画调整
            Column({ space: 10 }) {
              Button("1").width(100).height(50)
              Button("2").width(100).height(50)
              Button("3").width(100).height(50)
            }
            .margin(20)
            .alignItems(this.itemAlign)
            .borderWidth(2)
            .width("90%")
            .height(200)

            Button("click").onClick(() => {
              // 动画时长为1000ms，曲线为EaseInOut
              animateTo({ duration: 1000, curve: Curve.EaseInOut }, () => {
                this.alignIndex = (this.alignIndex + 1) % this.allAlign.length;
                // 在闭包函数中修改this.itemAlign参数，使Column容器内部孩子的布局方式变化，使用动画过渡到新位置
                this.itemAlign = this.allAlign[this.alignIndex];
              });
            })
            // 尺寸调整
            Button("text")
              .type(ButtonType.Normal)
              .width(this.myWidth)
              .height(this.myHeight)
              .margin(20)
            Button("area: click me")
              .fontSize(12)
              .margin(20)
              .onClick(() => {
                animateTo({ duration: 1000, curve: Curve.Ease }, () => {
                  // 动画闭包中根据标志位改变控制第一个Button宽高的状态变量，使第一个Button做宽高动画
                  if (this.flag) {
                    this.myWidth = 100;
                    this.myHeight = 50;
                  } else {
                    this.myWidth = 200;
                    this.myHeight = 100;
                  }
                  this.flag = !this.flag;
                });
              })
            CheckSourceButton({url:this.urlPath, title: this.title})
          }
        }
        .width("100%")
        .height("100%")
      }
      .title(this.title)
      .hideToolBar(true)
      .hideBackButton(false)
    }
}
```