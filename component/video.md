## 视频播放（Video）

```typescript

import router from '@ohos.router'
import { CheckSourceButton } from '../../common/CheckSourceButton'
@Entry
@Component
@Preview
struct VideoPage {

  @State title:string = ""
  @State urlPath:string = ""

  private controller:VideoController;
  private previewUris: Resource = $r('app.media.loading');
  private innerResource: Resource = $rawfile('video/baobao.mp4');

  aboutToAppear() {
    let param = router.getParams()
    this.title = param["title"]
    this.urlPath = param["urlPath"]
  }

  build() {
      Navigation() {
        List({space:10}) {
          ListItem() {
            Video({
              src: this.innerResource,
              previewUri: this.previewUris,
              controller: this.controller
            })
              .width('100%')
              .height('80%')
          }.margin({bottom: 20})
          ListItem() {
            CheckSourceButton({url:this.urlPath, title: this.title})
          }.margin({bottom: 15, top : 15, right: 15})
        }
        .padding(10)
      }
      .title(this.title)
      .hideToolBar(true)
      .hideBackButton(false)
    }
}
```