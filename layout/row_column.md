## 线性布局（Row/Column）
```typescript

import router from '@ohos.router'
import { CheckSourceButton } from '../../common/CheckSourceButton'
@Entry
@Component
@Preview
struct RowColumn {

  @State title:string = ""
  @State urlPath:string = ""

  aboutToAppear() {
    let param = router.getParams()
    this.title = param["title"]
    this.urlPath = param["urlPath"]
  }

  build() {
      Navigation() {
          List() {
            ListItem() {
              Text() {
                Span('Column().alignItems').fontSize(15).fontColor('#ff04cafa')
                Span('(').fontSize(15).fontColor('#fffa9804')
                Span('HorizontalAlign.Start').fontSize(15).fontColor('#ff0000')
                Span(')').fontSize(15).fontColor('#fffa9804')
              }
                .fontSize('14vp')
                .margin({ left: '10vp', top: '10vp', bottom: '5vp', right: '10vp' })
                .padding('10')
            }
            ListItem() {
              Column() {
                Text("1") {}.width('50%').height(50).backgroundColor(0xF5DEB3).textAlign(TextAlign.Center)
                Text("2") {}.width('50%').height(50).backgroundColor(0xD2B48C).textAlign(TextAlign.Center)
                Text("3") {}.width('50%').height(50).backgroundColor(0xF5DEB3).textAlign(TextAlign.Center)
              }
              .width('100%')
              .height(150)
              .alignItems(HorizontalAlign.Start)
              .backgroundColor('rgb(242,242,242)')
            }
            ListItem() {
              Text() {
                Span('Column().alignItems').fontSize(15).fontColor('#ff04cafa')
                Span('(').fontSize(15).fontColor('#fffa9804')
                Span('HorizontalAlign.Center').fontSize(15).fontColor('#ff0000')
                Span(')').fontSize(15).fontColor('#fffa9804')
              }
                .fontSize('14vp')
                .margin({ left: '10vp', top: '10vp', bottom: '5vp', right: '10vp' })
                .padding('10')
            }
            ListItem() {
              Column() {
                Text("1") {}.width('50%').height(50).backgroundColor(0xF5DEB3).textAlign(TextAlign.Center)
                Text("2") {}.width('50%').height(50).backgroundColor(0xD2B48C).textAlign(TextAlign.Center)
                Text("3") {}.width('50%').height(50).backgroundColor(0xF5DEB3).textAlign(TextAlign.Center)
              }
              .width('100%')
              .height(150)
              .alignItems(HorizontalAlign.Center)
              .backgroundColor('rgb(242,242,242)')
            }
            ListItem() {
              Text() {
                Span('Column().alignItems').fontSize(15).fontColor('#ff04cafa')
                Span('(').fontSize(15).fontColor('#fffa9804')
                Span('HorizontalAlign.End').fontSize(15).fontColor('#ff0000')
                Span(')').fontSize(15).fontColor('#fffa9804')
              }
                .fontSize('14vp')
                .margin({ left: '10vp', top: '10vp', bottom: '10vp', right: '10vp' })
                .padding('10')
            }
            ListItem() {
              Column() {
                Text("1") {}.width('50%').height(50).backgroundColor(0xF5DEB3).textAlign(TextAlign.Center)
                Text("2") {}.width('50%').height(50).backgroundColor(0xD2B48C).textAlign(TextAlign.Center)
                Text("3") {}.width('50%').height(50).backgroundColor(0xF5DEB3).textAlign(TextAlign.Center)
              }
              .width('100%')
              .height(150)
              .alignItems(HorizontalAlign.End)
              .backgroundColor('rgb(242,242,242)')
            }

            ListItem() {
              Text() {
                Span('Row().alignItems').fontSize(15).fontColor('#ff04cafa')
                Span('(').fontSize(15).fontColor('#fffa9804')
                Span('VerticalAlign.Top').fontSize(15).fontColor('#ff0000')
                Span(')').fontSize(15).fontColor('#fffa9804')
              }
                .fontSize('14vp')
                .margin({ left: '10vp', top: '10vp', bottom: '10vp', right: '10vp' })
                .padding('10')
            }
            ListItem() {
              Row({}) {
                Text("1") {}.width('20%').height(30).backgroundColor(0xF5DEB3).textAlign(TextAlign.Center)
                Text("2") {}.width('20%').height(30).backgroundColor(0xD2B48C).textAlign(TextAlign.Center)
                Text("3") {}.width('20%').height(30).backgroundColor(0xF5DEB3).textAlign(TextAlign.Center)
              }.width('100%').height(150).alignItems(VerticalAlign.Top)
              .backgroundColor('rgb(242,242,242)')
            }
            ListItem() {
              Text() {
                Span('Row().alignItems').fontSize(15).fontColor('#ff04cafa')
                Span('(').fontSize(15).fontColor('#fffa9804')
                Span('VerticalAlign.Center').fontSize(15).fontColor('#ff0000')
                Span(')').fontSize(15).fontColor('#fffa9804')
              }
                .fontSize('14vp')
                .margin({ left: '10vp', top: '10vp', bottom: '10vp', right: '10vp' })
                .padding('10')
            }
            ListItem() {
              Row({}) {
                Text("1") {}.width('20%').height(30).backgroundColor(0xF5DEB3).textAlign(TextAlign.Center)
                Text("2") {}.width('20%').height(30).backgroundColor(0xD2B48C).textAlign(TextAlign.Center)
                Text("3") {}.width('20%').height(30).backgroundColor(0xF5DEB3).textAlign(TextAlign.Center)
              }.width('100%').height(150).alignItems(VerticalAlign.Center)
              .backgroundColor('rgb(242,242,242)')
            }

            ListItem() {
              Text() {
                Span('Row().alignItems').fontSize(15).fontColor('#ff04cafa')
                Span('(').fontSize(15).fontColor('#fffa9804')
                Span('VerticalAlign.Bottom').fontSize(15).fontColor('#ff0000')
                Span(')').fontSize(15).fontColor('#fffa9804')
              }
                .fontSize('14vp')
                .margin({ left: '10vp', top: '10vp', bottom: '5vp', right: '10vp' })
                .padding('10')
            }
            ListItem() {
              Row({}) {
                Text("1") {}.width('20%').height(30).backgroundColor(0xF5DEB3).textAlign(TextAlign.Center)
                Text("2") {}.width('20%').height(30).backgroundColor(0xD2B48C).textAlign(TextAlign.Center)
                Text("3") {}.width('20%').height(30).backgroundColor(0xF5DEB3).textAlign(TextAlign.Center)
              }.width('100%').height(150).alignItems(VerticalAlign.Bottom)
              .backgroundColor('rgb(242,242,242)')
            }
            ListItem() {
              CheckSourceButton({url:this.urlPath, title: this.title})
            }.margin({bottom: 15, top : 15, right: 15})
          }
          .backgroundColor('#ffffffff')
      }
      .title(this.title)
      .hideToolBar(true)
      .hideBackButton(false)
    }
}
```