## 页面路由（router）

```typescript

import router from '@ohos.router'
import { CheckSourceButton } from '../../common/CheckSourceButton'
@Entry
@Component
@Preview
struct RouterLoginPage {

  @State title:string = ""
  @State urlPath:string = ""

  aboutToAppear() {
    let param = router.getParams()
    this.title = param["title"]
    this.urlPath = param["urlPath"]
  }

  build() {
      Navigation() {
        List({space:10}) {
          ListItem() {
            Text(){
              Span('router.replace').fontSize(15).fontColor('#ff04cafa')
              Span(`({`).fontSize(15).fontColor('#ff0000')
              Span(` url:\"pages/router/router_page\" `).fontSize(15)
              Span(`})`).fontSize(15).fontColor('#ff0000')
            }
          }.margin({top:10})
          ListItem() {
            Button('登录成功').onClick(()=>{
              router.replace({
                url:"pages/router/router_page"
              })
            })
          }.margin({top:5})

          ListItem() {
            CheckSourceButton({url:this.urlPath, title: this.title})
          }.margin({bottom: 15, top : 15, right: 15})
        }
        .padding(10)
      }
      .title(this.title)
      .hideToolBar(true)
      .hideBackButton(false)
    }
}
```