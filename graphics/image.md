## 显示图片（Image）

```typescript

import router from '@ohos.router'
import { CheckSourceButton } from '../../common/CheckSourceButton'
@Entry
@Component
@Preview
struct ImagePage {

  @State title:string = ""
  @State urlPath:string = ""

  imageHeight = 200

  scroller: Scroller = new Scroller()

  aboutToAppear() {
    let param = router.getParams()
    this.title = param["title"]
    this.urlPath = param["urlPath"]
  }

  build() {
      Navigation() {
        Scroll(this.scroller) {
          Column() {
              Image($r('app.media.fengjing')).width('100%').height(this.imageHeight)
                .border({ width: 1 })
                .objectFit(ImageFit.Contain).margin(15) // 保持宽高比进行缩小或者放大，使得图片完全显示在显示边界内。
                .overlay('Contain', { align: Alignment.Bottom, offset: { x: 0, y: 5 } })

              Image($r('app.media.fengjing')).width('100%').height(this.imageHeight)
                .border({ width: 1 })
                .objectFit(ImageFit.Cover).margin(15)
                // 保持宽高比进行缩小或者放大，使得图片两边都大于或等于显示边界。
                .overlay('Cover', { align: Alignment.Bottom, offset: { x: 0, y: 5 } })

              Image($r('app.media.fengjing')).width('100%').height(this.imageHeight)
                .border({ width: 1 })
                  // 自适应显示。
                .objectFit(ImageFit.Auto).margin(15)
                .overlay('Auto', { align: Alignment.Bottom, offset: { x: 0, y: 5 } })

              Image($r('app.media.fengjing')).width('100%').height(this.imageHeight)
                .border({ width: 1 })
                .objectFit(ImageFit.Fill).margin(15)
                // 不保持宽高比进行放大缩小，使得图片充满显示边界。
                .overlay('Fill', { align: Alignment.Bottom, offset: { x: 0, y: 5 } })

              Image($r('app.media.fengjing')).width('100%').height(this.imageHeight)
                .border({ width: 1 })
                  // 保持宽高比显示，图片缩小或者保持不变。
                .objectFit(ImageFit.ScaleDown).margin(15)
                .overlay('ScaleDown', { align: Alignment.Bottom, offset: { x: 0, y: 5 } })

              Image($r('app.media.fengjing')).width('100%').height(this.imageHeight)
                .border({ width: 1 })
                  // 保持原有尺寸显示。
                .objectFit(ImageFit.None).margin(15)
                .overlay('None', { align: Alignment.Bottom, offset: { x: 0, y: 5 } })

              CheckSourceButton({url:this.urlPath, title: this.title})
          }
        }
        .width('100%')
        .height('100%')
        .padding({bottom:20})
      }
      .title(this.title)
      .hideToolBar(true)
      .hideBackButton(false)
    }
}
```