## 绘制几何图形（Shape）

```typescript

import router from '@ohos.router'
import { CheckSourceButton } from '../../common/CheckSourceButton'
@Entry
@Component
@Preview
struct ShapePage {

  @State title:string = ""
  @State urlPath:string = ""

  imageHeight = 200

  scroller: Scroller = new Scroller()

  aboutToAppear() {
    let param = router.getParams()
    this.title = param["title"]
    this.urlPath = param["urlPath"]
  }

  build() {
      Navigation() {
        Scroll(this.scroller) {
          Column() {
              // 画一个宽高都为150的圆
              Text('原始尺寸Circle组件').fontSize(20).margin({top: 20, bottom: 10})
              Circle({width: 75, height: 75}).fill('#E87361')     // 红色 圆形


              // 创建一个宽高都为150的shape组件，背景色为黄色，一个宽高都为75的viewport。用一个蓝色的矩形来填充viewport，在viewport中绘制一个直径为75的圆。
              // 绘制结束，viewport会根据组件宽高放大两倍
              Text('创建一个宽高都为 150 的 shape 组件，背景色为黄色，一个宽高都为 75 的 viewport。用一个蓝色的矩形来填充viewport，在 viewport 中绘制一个直径为 75 的圆。').fontSize(15).margin({top:20, bottom: 10, left: 10, right: 10})
              Shape() {
                Rect().width('100%').height('100%').fill('#0097D4') // 蓝色 矩形
                Circle({width: 75, height: 75}).fill('#E87361')     // 红色 圆形
              }
              .viewPort({x: 0, y: 0, width: 75, height: 75})
              .width(150)
              .height(150)
              .backgroundColor('#F5DC62')

              // 创建一个宽高都为150的shape组件，背景色为黄色，一个宽高都为300的viewport。用一个绿色的矩形来填充viewport，在viewport中绘制一个直径为75的圆。
              // 绘制结束，viewport会根据组件宽高缩小两倍。
              Text('创建一个宽高都为 300 的 shape 组件，背景色为黄色，一个宽高都为 300 的 viewport。用一个蓝色的矩形来填充 viewport，在 viewport 中绘制一个半径为 75 的圆').fontSize(15).margin({top: 20, bottom: 10, left: 10, right: 10})
              Shape() {
                Rect().width('100%').height('100%').fill('#BDDB69')  // 青色 左上角矩形
                Circle({width: 75, height: 75}).fill('#E87361') // 红色 左上角圆形
              }
              .viewPort({x: 0, y: 0, width: 300, height: 300})
              .width(150)
              .height(150)
              .backgroundColor('#F5DC62') // 黄色

              Text('创建一个宽高都为 300 的 shape 组件，背景色为黄色，创建一个宽高都为 300 的 viewport。用一个蓝色的矩形来填充 viewport，在 viewport 中绘制一个半径为 75 的圆，将 viewport 向右方和下方各平移 150').fontSize(15).margin({top: 20, bottom: 10, left: 10, right: 10})
              Shape() {
                Rect().width("100%").height("100%").fill("#0097D4")
                Circle({ width: 75, height: 75 }).fill("#E87361")
              }
              .viewPort({ x: -150, y: -150, width: 300, height: 300 })
              .width(150)
              .height(150)
              .backgroundColor("#F5DC62")

              Text('通过fill可以设置组件填充区域颜色').fontSize(15).margin({top: 20, bottom: 10, left: 10, right: 10})
              Path()
                .width(100)
                .height(100)
                .commands('M150 0 L300 300 L0 300 Z')
                .fill("#E87361")

              Text('通过stroke可以设置组件边框颜色').fontSize(15).margin({top: 20, bottom: 10, left: 10, right: 10})
              Path()
                .width(100)
                .height(100)
                .fillOpacity(0)
                .commands('M150 0 L300 300 L0 300 Z')
                .stroke(Color.Red)

              CheckSourceButton({url:this.urlPath, title: this.title})
          }
        }
        .width('100%')
        .height('100%')
        .padding({bottom:20})
      }
      .title(this.title)
      .hideToolBar(true)
      .hideBackButton(false)
    }
}
```