## 层叠布局（Stack）

```typescript

import router from '@ohos.router'
import { CheckSourceButton } from '../../common/CheckSourceButton'
@Entry
@Component
@Preview
struct RowColumn {

  @State title:string = ""
  @State urlPath:string = ""

  aboutToAppear() {
    let param = router.getParams()
    this.title = param["title"]
    this.urlPath = param["urlPath"]
  }

  build() {
      Navigation() {
          List() {
            ListItem() {
              Text("Stack 默认参数")
                .fontSize('14vp')
                .margin({ left: '10vp', top: '10vp', bottom: '5vp', right: '10vp' })
                .padding('10')
            }
            ListItem() {
              Stack() {
                Text('Item1').width('100%').height('100%').backgroundColor('#ff58b87c')
                Text('Item2').width('70%').height('60%').backgroundColor('#ffc3f6aa')
                Text('Item3').width('30%').height('30%').backgroundColor('#ff8ff3eb').fontColor('#000')
              }.width('100%').height(150)
            }
            ListItem() {
              Text(){
                Span('Stack').fontSize(15).fontColor('#ff04cafa')
                Span('({').fontSize(15).fontColor('#fffa9804')
                Span('alignContent:Alignment.Top').fontSize(15).fontColor('#ff0000')
                Span('})').fontSize(15).fontColor('#fffa9804')
              }
                .fontSize('14vp')
                .margin({ left: '10vp', top: '10vp', bottom: '5vp', right: '10vp' })
                .padding('10')
            }
            ListItem() {
              Stack({alignContent:Alignment.Top}) {
                Text('Item1').width('100%').height('100%').backgroundColor('#ff58b87c').align(Alignment.Bottom)
                Text('Item2').width('70%').height('60%').backgroundColor('#ffc3f6aa').align(Alignment.Bottom)
                Text('Item3').width('30%').height('30%').backgroundColor('#ff8ff3eb').fontColor('#000')
              }.width('100%').height(150)
            }
            ListItem() {
              Text() {
                Span('Stack').fontSize(15).fontColor('#ff04cafa')
                Span('({').fontSize(15).fontColor('#fffa9804')
                Span('alignContent:Alignment.TopStart').fontSize(15).fontColor('#ff0000')
                Span('})').fontSize(15).fontColor('#fffa9804')
              }
                .fontSize('14vp')
                .margin({ left: '10vp', top: '10vp', bottom: '5vp', right: '10vp' })
                .padding('10')
            }
            ListItem() {
              Stack({alignContent:Alignment.TopStart}) {
                Text('Item1').width('100%').height('100%').backgroundColor('#ff58b87c').align(Alignment.Bottom)
                Text('Item2').width('70%').height('60%').backgroundColor('#ffc3f6aa').align(Alignment.Bottom)
                Text('Item3').width('30%').height('30%').backgroundColor('#ff8ff3eb').fontColor('#000')
              }.width('100%').height(150)
            }
            ListItem() {
              Text() {
                Span('Stack').fontSize(15).fontColor('#ff04cafa')
                Span('({').fontSize(15).fontColor('#fffa9804')
                Span('alignContent:Alignment.TopEnd').fontSize(15).fontColor('#ff0000')
                Span('})').fontSize(15).fontColor('#fffa9804')
              }
                .fontSize('14vp')
                .margin({ left: '10vp', top: '10vp', bottom: '5vp', right: '10vp' })
                .padding('10')
            }
            ListItem() {
              Stack({alignContent:Alignment.TopEnd}) {
                Text('Item1').width('100%').height('100%').backgroundColor('#ff58b87c').align(Alignment.Bottom)
                Text('Item2').width('70%').height('60%').backgroundColor('#ffc3f6aa').align(Alignment.Bottom)
                Text('Item3').width('30%').height('30%').backgroundColor('#ff8ff3eb').fontColor('#000')
              }.width('100%').height(150)
            }
            ListItem() {
              Text() {
                Span('Stack').fontSize(15).fontColor('#ff04cafa')
                Span('({').fontSize(15).fontColor('#fffa9804')
                Span('alignContent:Alignment.Center').fontSize(15).fontColor('#ff0000')
                Span('})').fontSize(15).fontColor('#fffa9804')
              }
                .fontSize('14vp')
                .margin({ left: '10vp', top: '10vp', bottom: '5vp', right: '10vp' })
                .padding('10')
            }
            ListItem() {
              Stack({alignContent:Alignment.Center}) {
                Text('Item1').width('100%').height('100%').backgroundColor('#ff58b87c')
                Text('Item2').width('70%').height('60%').backgroundColor('#ffc3f6aa')
                Text('Item3').width('30%').height('30%').backgroundColor('#ff8ff3eb').fontColor('#000')
              }.width('100%').height(150)
            }
            ListItem() {
              Text() {
                Span('Stack').fontSize(15).fontColor('#ff04cafa')
                Span('({').fontSize(15).fontColor('#fffa9804')
                Span('alignContent:Alignment.Bottom').fontSize(15).fontColor('#ff0000')
                Span('})').fontSize(15).fontColor('#fffa9804')
              }
                .fontSize('14vp')
                .margin({ left: '10vp', top: '10vp', bottom: '5vp', right: '10vp' })
                .padding('10')
            }
            ListItem() {
              Stack({alignContent:Alignment.Bottom}) {
                Text('Item1').width('100%').height('100%').backgroundColor('#ff58b87c').align(Alignment.Top)
                Text('Item2').width('70%').height('60%').backgroundColor('#ffc3f6aa').align(Alignment.Top)
                Text('Item3').width('30%').height('30%').backgroundColor('#ff8ff3eb').fontColor('#000').align(Alignment.Top)
              }.width('100%').height(150)
            }
            ListItem() {
              Text() {
                Span('Stack').fontSize(15).fontColor('#ff04cafa')
                Span('({').fontSize(15).fontColor('#fffa9804')
                Span('alignContent:Alignment.BottomStart').fontSize(15).fontColor('#ff0000')
                Span('})').fontSize(15).fontColor('#fffa9804')
              }
                .fontSize('14vp')
                .margin({ left: '10vp', top: '10vp', bottom: '5vp', right: '10vp' })
                .padding('10')
            }
            ListItem() {
              Stack({alignContent:Alignment.BottomStart}) {
                Text('Item1').width('100%').height('100%').backgroundColor('#ff58b87c').align(Alignment.Top)
                Text('Item2').width('70%').height('60%').backgroundColor('#ffc3f6aa').align(Alignment.Top)
                Text('Item3').width('30%').height('30%').backgroundColor('#ff8ff3eb').fontColor('#000').align(Alignment.Top)
              }.width('100%').height(150)
            }
            ListItem() {
              Text() {
                Span('Stack').fontSize(15).fontColor('#ff04cafa')
                Span('({').fontSize(15).fontColor('#fffa9804')
                Span('alignContent:Alignment.BottomEnd').fontSize(15).fontColor('#ff0000')
                Span('})').fontSize(15).fontColor('#fffa9804')
              }
                .fontSize('14vp')
                .margin({ left: '10vp', top: '10vp', bottom: '5vp', right: '10vp' })
                .padding('10')
            }
            ListItem() {
              Stack({alignContent:Alignment.BottomEnd}) {
                Text('Item1').width('100%').height('100%').backgroundColor('#ff58b87c').align(Alignment.Top)
                Text('Item2').width('70%').height('60%').backgroundColor('#ffc3f6aa').align(Alignment.Top)
                Text('Item3').width('30%').height('30%').backgroundColor('#ff8ff3eb').fontColor('#000').align(Alignment.Top)
              }.width('100%').height(150)
            }
            ListItem() {
              CheckSourceButton({url:this.urlPath, title: this.title})
            }.margin({bottom: 15, top : 15, right: 15})
          }
          .backgroundColor('#ffffffff')
      }
      .title(this.title)
      .hideToolBar(true)
      .hideBackButton(false)
    }
}
```