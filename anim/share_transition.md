## 组件内转场动画


### 起始页
```typescript

import router from '@ohos.router'
import { CheckSourceButton } from '../../common/CheckSourceButton'
@Entry
@Component
@Preview
struct ShareTransitionPage {

  @State title:string = ""
  @State urlPath:string = ""

  aboutToAppear() {
    let param = router.getParams()
    this.title = param["title"]
    this.urlPath = param["urlPath"]
  }

  build() {
      Navigation() {
        Column() {
          // 配置Exchange类型的共享元素转场，共享元素id为"sharedImage1"
          Image($r('app.media.fengjing')).width(50).height(50)
            .sharedTransition('sharedImage1', { duration: 1000, curve: Curve.Linear })
            .onClick(() => {
              // 点击小图时路由跳转至下一页面
              router.push({ url: 'pages/anim/share_transition_dest',
                params: {
                  title:'动画目标页'
                }
              });
            })

          CheckSourceButton({url:this.urlPath, title: this.title})
        }
        .padding(10)
        .width("100%")
        .alignItems(HorizontalAlign.Start)
      }
      .title(this.title)
      .hideToolBar(true)
      .hideBackButton(false)
    }
}
```

### 目标页
```typescript
import router from '@ohos.router'
import { CheckSourceButton } from '../../common/CheckSourceButton'
@Entry
@Component
@Preview
struct ShareTransitionDestPage {

  @State title:string = ""
  @State urlPath:string = ""

  aboutToAppear() {
    let param = router.getParams()
    this.title = param["title"]
    this.urlPath = param["urlPath"]
  }

  build() {
      Navigation() {
        Column() {
          Column() {
            // 配置Static类型的共享元素转场
            Text("SharedTransition dest page")
              .fontSize(16)
              .sharedTransition('text', { duration: 500, curve: Curve.Linear, type: SharedTransitionEffectType.Static })
              .margin({ top: 10 })

            // 配置Exchange类型的共享元素转场，共享元素id为"sharedImage1"
            Image($r('app.media.fengjing'))
              .width(150)
              .height(150)
              .sharedTransition('sharedImage1', { duration: 500, curve: Curve.Linear })
              .onClick(() => {
                // 点击图片时路由返回至上一页面
                router.back();
              })
          }
          .width("100%")
          .alignItems(HorizontalAlign.Center)
        }
        .padding(10)
        .width("100%")
        .alignItems(HorizontalAlign.Start)
      }
      .title(this.title)
      .hideToolBar(true)
      .hideBackButton(false)
    }
}
```
