## 页面路由（router）

```typescript


import router from '@ohos.router'
import { CheckSourceButton } from '../../common/CheckSourceButton'
@Entry
@Component
@Preview
struct RouterPage {

  @State title:string = ""
  @State urlPath:string = ""

  aboutToAppear() {
    let param = router.getParams()
    this.title = param["title"]
    this.urlPath = param["urlPath"]
  }

  build() {
      Navigation() {
        List({space:10}) {
          ListItem() {
            Text(){
              Span('router.push').fontSize(15).fontColor('#ff04cafa')
              Span(` ({`).fontSize(15).fontColor('#ff0000')
              Span('url:\'pages/router/router_page_detail\'').fontSize(15)
              Span(` })`).fontSize(15).fontColor('#ff0000')
            }
          }.margin({top:10})
          ListItem() {
            Button('跳转 Detail').onClick(()=>{
                router.push({
                  url: "pages/router/router_page_detail",
                  params: {
                    title: "Detail"
                  }
                })
            })
          }.margin({top:5})

          ListItem() {
            Text(){
              Span('router.push').fontSize(15).fontColor('#ff04cafa')
              Span(` ({`).fontSize(15).fontColor('#ff0000')
              Span('url:\'pages/router/router_page_login\'').fontSize(15)
              Span(` })`).fontSize(15).fontColor('#ff0000')
            }
          }.margin({top:10})
          ListItem() {
            Button('跳转 Login').onClick(()=>{
              router.push({
                url: "pages/router/router_page_login",
                params: {
                  title: "Login"
                }
              })
            })
          }.margin({top:5})

          ListItem() {
            CheckSourceButton({url:this.urlPath, title: this.title})
          }.margin({bottom: 15, top : 15, right: 15})
        }
        .padding(10)
      }
      .title(this.title)
      .hideToolBar(true)
      .hideBackButton(false)
    }
}
```