## 进度条（Progress）

```typescript

import router from '@ohos.router'
import { CheckSourceButton } from '../../common/CheckSourceButton'
@Entry
@Component
@Preview
struct ProgressPage {

  @State title:string = ""
  @State urlPath:string = ""

  @State checkbox1:boolean = true
  @State checkbox2:boolean = false
  @State checkbox3:boolean = false
  @State checkbox4:boolean = false

  aboutToAppear() {
    let param = router.getParams()
    this.title = param["title"]
    this.urlPath = param["urlPath"]
  }

  build() {
      Navigation() {
        List({space:10}) {
          ListItem() {
            Text(){
              Span('Progress').fontSize(15).fontColor('#ff04cafa')
              Span(`({`).fontSize(15).fontColor('#ff0000')
              Span(' value: 24, total: 100, type: ProgressType.Linear ').fontSize(15)
              Span(`})`).fontSize(15).fontColor('#ff0000')
            }
          }.margin({top:10})
          ListItem() {
             Progress({ value: 24, total: 100, type: ProgressType.Linear }) // 创建一个进度总长为100，初始进度值为24的线性进度条
          }.margin({top:10})
          ListItem() {
            Text(){
              Span('Progress').fontSize(15).fontColor('#ff04cafa')
              Span(`({`).fontSize(15).fontColor('#ff0000')
              Span(' value: 24, total: 100, type: ProgressType.Ring ').fontSize(15)
              Span(`})`).fontSize(15).fontColor('#ff0000')
            }
          }.margin({top:10})
          ListItem() {
             Progress({ value: 24, total: 100, type: ProgressType.Ring }) // 创建一个进度总长为100，初始进度值为24的线性进度条
          }.margin({top:10})
          ListItem() {
            Progress({ value: 24, total: 100, type: ProgressType.Ring }) // 创建一个进度总长为100，初始进度值为24的线性进度条
              .color(Color.Grey)    // 进度条前景色为灰色
              .style({ strokeWidth: 15})    // 设置strokeWidth进度条宽度为15.0vp
          }.margin({top:10})


          ListItem() {
            Text(){
              Span('Progress').fontSize(15).fontColor('#ff04cafa')
              Span(`({`).fontSize(15).fontColor('#ff0000')
              Span(' value: 24, total: 100, type: ProgressType.ScaleRing ').fontSize(15)
              Span(`})`).fontSize(15).fontColor('#ff0000')
            }
          }.margin({top:10})
          ListItem() {
            Progress({ value: 24, total: 100, type: ProgressType.ScaleRing }).width(100).height(100)
              .backgroundColor(Color.Black)
              .style({ scaleCount: 20, scaleWidth: 5 })    // 设置环形有刻度进度条总刻度数为20，刻度宽度为5vp
          }.margin({top:10})
          ListItem() {
            Progress({ value: 24, total: 100, type: ProgressType.ScaleRing }).width(100).height(100)
              .backgroundColor(Color.Black)
              .style({ strokeWidth: 15, scaleCount: 20, scaleWidth: 5 })    // 设置环形有刻度进度条宽度15，总刻度数为20，刻度宽度为5vp
          }.margin({top:10})
          ListItem() {
            Progress({ value: 24, total: 100, type: ProgressType.ScaleRing }).width(100).height(100)
              .backgroundColor(Color.Black)
              .style({ strokeWidth: 15, scaleCount: 20, scaleWidth: 3 })    // 设置环形有刻度进度条宽度15，总刻度数为20，刻度宽度为3vp
          }.margin({top:10})
          ListItem() {
            Text(){
              Span('Progress').fontSize(15).fontColor('#ff04cafa')
              Span(`({`).fontSize(15).fontColor('#ff0000')
              Span(' value: 24, total: 100, type: ProgressType.Eclipse ').fontSize(15)
              Span(`})`).fontSize(15).fontColor('#ff0000')
            }
          }.margin({top:10})
          ListItem() {
            // 从左往右，1号圆形进度条，默认前景色为蓝色
            Progress({ value: 24, total: 100, type: ProgressType.Eclipse }).width(100).height(100)
          }.margin({top:10})
          ListItem() {
            // 从左往右，2号圆形进度条，指定前景色为灰色
            Progress({ value: 24, total: 100, type: ProgressType.Eclipse }).color(Color.Grey).width(100).height(100)
          }.margin({top:10})

          ListItem() {
            Text(){
              Span('Progress').fontSize(15).fontColor('#ff04cafa')
              Span(`({`).fontSize(15).fontColor('#ff0000')
              Span(' value: 24, total: 100, type: ProgressType.Capsule ').fontSize(15)
              Span(`})`).fontSize(15).fontColor('#ff0000')
            }
          }.margin({top:10})
          ListItem() {
            Progress({ value: 24, total: 100, type: ProgressType.Capsule }).width(100).height(50)
          }.margin({top:10})

          ListItem() {
            CheckSourceButton({url:this.urlPath, title: this.title})
          }.margin({bottom: 15, top : 15, right: 15})
        }
        .padding(10)
      }
      .title(this.title)
      .hideToolBar(true)
      .hideBackButton(false)
    }
}
```