## 页面路由（router）

```typescript


import router from '@ohos.router'
import { CheckSourceButton } from '../../common/CheckSourceButton'
@Entry
@Component
@Preview
struct RouterDetailPage {

  @State title:string = ""
  @State urlPath:string = ""

  aboutToAppear() {
    let param = router.getParams()
    this.title = param["title"]
    this.urlPath = param["urlPath"]
  }

  build() {
      Navigation() {
        List({space:10}) {
          ListItem() {
            Text(){
              Span('router.back').fontSize(15).fontColor('#ff04cafa')
              Span(`(`).fontSize(15).fontColor('#ff0000')
              Span(`)`).fontSize(15).fontColor('#ff0000')
            }
          }.margin({top:10})
          ListItem() {
            Button('返回跳转').onClick(()=>{
              router.back()
            })
          }.margin({top:5})

          ListItem() {
            CheckSourceButton({url:this.urlPath, title: this.title})
          }.margin({bottom: 15, top : 15, right: 15})
        }
        .padding(10)
      }
      .title(this.title)
      .hideToolBar(true)
      .hideBackButton(false)
    }
}
```