## 媒体查询（mediaquery）

```typescript
import router from '@ohos.router'
import mediaquery from '@ohos.mediaquery';
import featureAbility from '@ohos.ability.featureAbility';
import bundle from '@ohos.bundle';

let portraitFunc = null;

@Entry
@Component
@Preview
struct MediaQueryPage {

  @State title:string = ""
  @State urlPath:string = ""

  @State text: string = '';

  listener = mediaquery.matchMediaSync('(orientation: landscape)');


  aboutToAppear() {
    let param = router.getParams()
    this.title = param["title"]
    this.urlPath = param["urlPath"]

    // 绑定当前应用实例
    portraitFunc = this.onPortrait.bind(this);
    // 绑定回调函数
    this.listener.on('change', portraitFunc);
  }

  // 当满足媒体查询条件时，触发回调
  onPortrait(mediaQueryResult) {
    if (mediaQueryResult.matches) { // 若设备为横屏状态，更改相应的页面布局
      this.text = '横屏';
    } else {
      this.text = '竖屏';
    }
  }


  build() {
      Navigation() {
          List() {
              ListItem() {
                Column({ space: 50 }) {
                  Text(this.text).fontSize(50)
                  Button('切换横屏')
                    .onClick(() => {
                      let context = featureAbility.getContext();
                      context.setDisplayOrientation(bundle.DisplayOrientation.LANDSCAPE); //调用该接口手动改变设备横竖屏状态
                    })
                  Button('切换竖屏')
                    .onClick(() => {
                      let context = featureAbility.getContext();
                      context.setDisplayOrientation(bundle.DisplayOrientation.PORTRAIT); //调用该接口手动改变设备横竖屏状态
                    })
                }.width('100%')
              }
          }
          .width('100%')
          .backgroundColor('#ffffffff')
      }
      .title(this.title)
      .hideToolBar(true)
      .hideBackButton(false)
    }
}
```