## 文本输入（TextInput/TextArea）

```typescript

import router from '@ohos.router'
import { CheckSourceButton } from '../../common/CheckSourceButton'
@Entry
@Component
@Preview
struct InputPage {

  @State title:string = ""
  @State urlPath:string = ""

  @State value:string = ""

  aboutToAppear() {
    let param = router.getParams()
    this.title = param["title"]
    this.urlPath = param["urlPath"]
  }

  build() {
      Navigation() {
        List({space:10}) {
          ListItem() {
            Text(){
              Span('基本输入模式（默认类型）').fontSize(15)
            }
          }.margin({top:10})
          ListItem() {
            TextInput()
              .type(InputType.Normal)
          }.margin({top:5})

          ListItem() {
            Text(){
              Span('密码输入模式').fontSize(15)
            }
          }.margin({top:20})
          ListItem() {
            TextInput()
              .type(InputType.Password)
          }.margin({top:5})

          ListItem() {
            Text(){
              Span(`添加事件 输入变化：${this.value}`).fontSize(15)
            }
          }.margin({top:20})
          ListItem() {
            TextInput()
              .onChange((value: string) => {
                this.value = value
                console.info(`InputPage ${value}`);
              })
              .onFocus(() => {
                console.info('InputPage 获取焦点');
              })
              .onDisAppear(()=> {
                console.info('InputPage onDisAppear');
              })
          }.margin({top:5})

          ListItem() {
            Text(){
              Span('设置无输入时的提示文本').fontSize(15)
            }
          }.margin({top:20})
          ListItem() {
            TextInput({placeholder:'我是提示文本'})
          }.margin({top:5})

          ListItem() {
            Text(){
              Span('设置输入框当前的文本内容').fontSize(15)
            }
          }.margin({top:20})
          ListItem() {
            TextInput({placeholder:'我是提示文本',text:'我是当前文本内容'})
          }.margin({top:5})

          ListItem() {
            Text(){
              Span('添加 backgroundColor 改变输入框的背景颜色').fontSize(15)
            }
          }.margin({top:20})
          ListItem() {
            TextInput({placeholder:'我是提示文本',text:'我是当前文本内容'})
              .backgroundColor(Color.Pink)
          }.margin({top:5})

          ListItem() {
            CheckSourceButton({url:this.urlPath, title: this.title})
          }.margin({bottom: 550, top : 15, right: 15})
        }
        .padding(10)
      }
      .title(this.title)
      .hideToolBar(true)
      .hideBackButton(false)
    }
}
```