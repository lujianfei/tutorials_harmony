## 单选框（Radio）

```typescript

import router from '@ohos.router'
import { CheckSourceButton } from '../../common/CheckSourceButton'
@Entry
@Component
@Preview
struct RadioPage {

  @State title:string = ""
  @State urlPath:string = ""

  @State radioSelectIdx:number = 0

  aboutToAppear() {
    let param = router.getParams()
    this.title = param["title"]
    this.urlPath = param["urlPath"]
  }

  build() {
      Navigation() {
        List({space:10}) {
          ListItem() {
            Column() {
              Text() {
                Span('Radio').fontSize(15).fontColor('#ff04cafa')
                Span('({').fontSize(15).fontColor('#fffa9804')
                Span('value: \'Radio1\', group: \'radioGroup\'').fontSize(15).fontColor('#ff0000')
                Span('})').fontSize(15).fontColor('#fffa9804')
              }
              Text() {
                Span('Radio').fontSize(15).fontColor('#ff04cafa')
                Span('({').fontSize(15).fontColor('#fffa9804')
                Span('value: \'Radio2\', group: \'radioGroup\'').fontSize(15).fontColor('#ff0000')
                Span('})').fontSize(15).fontColor('#fffa9804')
              }
            }
          }
          ListItem() {
            Text(`你选择了 Radio${this.radioSelectIdx+1}`).fontSize(20)
          }.margin({top:10})
          ListItem() {
            Row() {
              Radio({ value: 'Radio1', group: 'radioGroup' })
                .checked(this.radioSelectIdx == 0)
                .onChange((isChecked:boolean)=> {
                  if (isChecked) {
                    this.radioSelectIdx = 0
                  }
                })
              Text('Radio1')
                .fontSize(20)
                .onClick(()=>{
                  this.radioSelectIdx = 0
                })
              Radio({ value: 'Radio2', group: 'radioGroup' })
                .checked(this.radioSelectIdx == 1)
                .onChange((isChecked:boolean)=> {
                  if (isChecked) {
                    this.radioSelectIdx = 1
                  }
                })
              Text('Radio2')
                .fontSize(20)
                .onClick(()=>{
                  this.radioSelectIdx = 1
              })
            }
          }.margin({bottom: 20})
          ListItem() {
            CheckSourceButton({url:this.urlPath, title: this.title})
          }.margin({bottom: 15, top : 15, right: 15})
        }
        .padding(10)
      }
      .title(this.title)
      .hideToolBar(true)
      .hideBackButton(false)
    }
}
```