## 创建轮播（Swiper）

```typescript

import router from '@ohos.router'
import { CheckSourceButton } from '../../common/CheckSourceButton'
@Entry
@Component
@Preview
struct SwiperPage {

  @State title:string = ""
  @State urlPath:string = ""

  @State dataList:string[] = []

  private swiperController: SwiperController = new SwiperController()

  aboutToAppear() {
    let param = router.getParams()
    this.title = param["title"]
    this.urlPath = param["urlPath"]

    for (let index = 0; index < 5; index++) {
      this.dataList.push(`Item ${index + 1}`)
    }
  }

  build() {
      Navigation() {
        List() {
          ListItem() {
            Swiper(this.swiperController) {
              ForEach(this.dataList, (item:string)=> {
                Text(item)
                  .width('100%')
                  .height(300)
                  .backgroundColor(Color.Gray)
                  .textAlign(TextAlign.Center)
                  .fontSize(30)
              })
            }
            .loop(true) // 循环列表
            .autoPlay(true) // 自动播放
          }
          ListItem() {
            CheckSourceButton({url:this.urlPath, title: this.title})
          }.margin({bottom: 15, top : 15, right: 15})
        }
      }
      .title(this.title)
      .hideToolBar(true)
      .hideBackButton(false)
    }
}
```