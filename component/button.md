## 按钮（Button）

```typescript

import router from '@ohos.router'
import { CheckSourceButton } from '../../common/CheckSourceButton'
@Entry
@Component
@Preview
struct ButtonPage {

  @State title:string = ""
  @State urlPath:string = ""


  aboutToAppear() {
    let param = router.getParams()
    this.title = param["title"]
    this.urlPath = param["urlPath"]
  }

  build() {
      Navigation() {
        List({space:10}) {
          ListItem() {
            Text() {
              Span('Button').fontSize(15).fontColor('#ff04cafa')
              Span('({').fontSize(15).fontColor('#fffa9804')
              Span('type: ButtonType.Normal, stateEffect: true').fontSize(15).fontColor('#ff0000')
              Span('})').fontSize(15).fontColor('#fffa9804')
            }
          }
          ListItem() {
            Button('Ok', { type: ButtonType.Normal, stateEffect: true })
              .borderRadius(8)
              .backgroundColor(0x317aff)
              .width(90)
              .height(40)
          }.margin({bottom: 20})
          ListItem() {
            Text() {
              Span('创建包含子组件的按钮').fontSize(15).fontColor('#ff04cafa')
            }
          }
          ListItem() {
            Button({ type: ButtonType.Normal, stateEffect: true }) {
              Row() {
                Image($r('app.media.loading')).width(20).height(20).margin({ left: 12 })
                Text('loading').fontSize(12).fontColor(0xffffff).margin({ left: 5, right: 12 })
              }.alignItems(VerticalAlign.Center)
            }.borderRadius(8).backgroundColor(0x317aff).width(90).height(40)
          }.margin({bottom: 20})
          ListItem() {
            Text() {
              Span('Button').fontSize(15).fontColor('#ff04cafa')
              Span('({').fontSize(15).fontColor('#fffa9804')
              Span('type: ButtonType.Capsule, stateEffect: false').fontSize(15).fontColor('#ff0000')
              Span('})').fontSize(15).fontColor('#fffa9804')
            }
          }
          ListItem() {
            Button('Disable', { type: ButtonType.Capsule, stateEffect: false })
              .backgroundColor(0x317aff)
              .width(90)
              .height(40)
          }.margin({bottom: 20})
          ListItem() {
            Text() {
              Span('Button').fontSize(15).fontColor('#ff04cafa')
              Span('({').fontSize(15).fontColor('#fffa9804')
              Span('type: ButtonType.Circle, stateEffect: false').fontSize(15).fontColor('#ff0000')
              Span('})').fontSize(15).fontColor('#fffa9804')
            }
          }
          ListItem() {
            Button('Circle', { type: ButtonType.Circle, stateEffect: false })
              .backgroundColor(0x317aff)
              .width(90)
              .height(90)
          }.margin({bottom: 20})
          ListItem() {
            Text() {
              Span('设置边框弧度').fontSize(15).fontColor('#ff04cafa')
              Span(' borderRadius(20)').fontSize(15).fontColor('#ff0000')
            }
          }
          ListItem() {
            Button('circle border', { type: ButtonType.Normal })
              .borderRadius(20)
              .height(40)
          }.margin({bottom: 20})
          ListItem() {
            Text() {
              Span('设置文本样式').fontSize(15).fontColor('#ff04cafa')
              Span('\n fontColor(Color.Pink)').fontSize(15).fontColor('#ff0000')
              Span('\n fontWeight(800)').fontSize(15).fontColor('#ff0000')
            }
          }
          ListItem() {
            Button('font style', { type: ButtonType.Normal })
              .fontSize(20)
              .fontColor(Color.Pink)
              .fontWeight(800)
          }.margin({bottom: 20})
          ListItem() {
            Text() {
              Span('设置背景颜色').fontSize(15).fontColor('#ff04cafa')
              Span('\n backgroundColor(0xF55A42)').fontSize(15).fontColor('#ff0000')
            }
          }.margin({top:15})
          ListItem() {
            Button('background color').backgroundColor(0xF55A42)
          }.margin({bottom: 20})
          ListItem() {
            Text() {
              Span('用作功能型按钮').fontSize(15).fontColor('#ff04cafa')
            }
          }
          ListItem() {
            Button({ type: ButtonType.Circle, stateEffect: true }) {
              Image($r('app.media.delete_fill')).width(30).height(30)
            }.width(55).height(55).margin({ left: 20 }).backgroundColor(0xF55A42)
          }.margin({bottom: 20})
          ListItem() {
            CheckSourceButton({url:this.urlPath, title: this.title})
          }.margin({bottom: 15, top : 15, right: 15})
        }
        .padding(10)
      }
      .title(this.title)
      .hideToolBar(true)
      .hideBackButton(false)
    }
}
```