## 点击事件

```typescript

import router from '@ohos.router'
import { CheckSourceButton } from '../../common/CheckSourceButton'
@Entry
@Component
@Preview
struct ClientEventPage {

  @State title:string = ""
  @State urlPath:string = ""

  @State flag: boolean = true;
  @State btnMsg: string = 'show';


  aboutToAppear() {
    let param = router.getParams()
    this.title = param["title"]
    this.urlPath = param["urlPath"]
  }

  build() {
      Navigation() {
        Column() {
          Button(this.btnMsg).width(80).height(30).margin(30)
            .onClick(() => {
              if (this.flag) {
                this.btnMsg = 'hide';
              } else {
                this.btnMsg = 'show';
              }
              // 点击Button控制Image的显示和消失
              this.flag = !this.flag;
            })
          if (this.flag) {
            Image($r('app.media.icon')).width(200).height(200)
          }
          CheckSourceButton({url:this.urlPath, title: this.title, margin:{right: 20, top : 20}})
        }.height('100%').width('100%')
      }
      .title(this.title)
      .hideToolBar(true)
      .hideBackButton(false)
    }
}
```